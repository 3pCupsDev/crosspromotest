using System.Collections;
using UnityEngine;
using System;
using System.IO;
using UnityEngine.Networking;
using System.Web;
using System.Net;

interface ICrossPromoNetworkCalls
{
    public PromotionsPlaylistResponse GetPlaylist();
    public void ReportAnalytics(string analyticsUrl);
}

public class NetworkManager : MonoBehaviour, ICrossPromoNetworkCalls
{
    public PromotionsPlaylistResponse GetPlaylist()
    {
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Constants.BASE_API_URL);
        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        StreamReader reader = new StreamReader(response.GetResponseStream());
        string jsonResponse = reader.ReadToEnd();
        PromotionsPlaylistResponse info = JsonUtility.FromJson<PromotionsPlaylistResponse>(jsonResponse);
        return info;
    }

    public void ReportAnalytics(string analyticsUrl)
    {
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(analyticsUrl);
        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        StreamReader reader = new StreamReader(response.GetResponseStream());
        string strResponse = reader.ReadToEnd();
        Debug.Log("analytics server repsonse is:" + strResponse);
    }
}

