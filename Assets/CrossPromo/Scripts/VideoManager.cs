using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;

interface IVideoManagerPublicAPI
{
    public void Next();
    public void Previous();
    public void Pause();
    public void Resume();
}

interface ICrossPromoVideoController
{
    public Promotion GetCurrentPromotion();
    public void SubmitList(List<Promotion> NewPromotionsList);

    public void ToggleStubData();
}

public class VideoManager : MonoBehaviour, IVideoManagerPublicAPI, ICrossPromoVideoController
{
    private VideoPlayer VideoPlayer;
    private bool isPlaying = false;

    private List<Promotion> promotionsPlaylist;
    private int currentPromotionIndex = -1;

    [SerializeField]
    public Text LoadingText;

    /*
     * Offline workaround
     */
    [SerializeField]
    public bool isStubData = true;
    [SerializeField]
    public string[] StubVideoUrls = new string[] {
        Constants.DEFAULT_MP4_VIDEO_URL, 
        Constants.DEFAULT_MP4_VIDEO_URL_2, 
        Constants.DEFAULT_MP4_VIDEO_URL_3
    };

    void Awake()
    {
        VideoPlayer = GetComponent<VideoPlayer>();
        LoadingText = GetComponentInChildren<Text>();
    }

    void Start()
    {
        VideoPlayer.prepareCompleted += VideoLoaded;
    }

    void LateUpdate()
    {
        LoadingText.enabled = !isPlaying || !VideoPlayer.isPrepared || !VideoPlayer.isPlaying;
    }

    /*
     * Public CrossPromoVideoController implementation
     */
    public void SubmitList(List<Promotion> newPromotionsList)
    {
        promotionsPlaylist = newPromotionsList;
        if(isPlaying)
            Next();
    }

    public Promotion GetCurrentPromotion()
    {
        return promotionsPlaylist[currentPromotionIndex];
    }

    public void ToggleStubData()
    {
        isStubData = !isStubData;
        if (isPlaying)
            Next();
        Debug.Log("ToggleStubData");
    }


    /*
     * 
     * Public API Functions
     *
     **/
    public void Next()
    {
        if(currentPromotionIndex == promotionsPlaylist.Count - 1)
        {
            currentPromotionIndex = 0;
        } else
        {
            currentPromotionIndex = Mathf.Min(currentPromotionIndex + 1, promotionsPlaylist.Count - 1);
        }
        Promotion nextPromotion = promotionsPlaylist[currentPromotionIndex];
        PlayUrl(nextPromotion.video_url);
    }

    public void Previous()
    {
        if (currentPromotionIndex == 0)
        {
            currentPromotionIndex = promotionsPlaylist.Count - 1;
        }
        else
        {
            currentPromotionIndex = Mathf.Max(currentPromotionIndex - 1, 0);
        }
        Promotion previousPromotion = promotionsPlaylist[currentPromotionIndex];
        PlayUrl(previousPromotion.video_url);
    }

    public void Pause()
    {
        if (isPlaying)
        {
            VideoPlayer.Pause();
            Debug.Log("Pause");
        }
        isPlaying = false;
    }

    public void Resume()
    {
        if (!isPlaying)
        {
            VideoPlayer.Play();
            Debug.Log("Resume");
        }
        isPlaying = true;
    }


    /*
     * Private helper function 
     */
    private void PlayUrl(string url)
    {
        string videoUrl = url;
        if(isStubData)
        {
            videoUrl = StubVideoUrls[currentPromotionIndex];
        }
        VideoPlayer.url = System.Web.HttpUtility.UrlDecode(videoUrl);
        VideoPlayer.Prepare();
        VideoPlayer.Play();

        Debug.Log("videoUrl is:" + videoUrl);
        Debug.Log("currentPromotionIndex is:" + currentPromotionIndex);
    }

    /*
     * callback from VideoPlayer
     */
    void VideoLoaded(UnityEngine.Video.VideoPlayer vp)
    {
        isPlaying = true;
        Debug.Log("video started");
    }
}
