using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants
{
    public const string PLAYER_ID_TAG = "[PLAYER_ID]";

    public const string BASE_API_URL = "https://run.mocky.io/v3/75deb965-b835-4591-b8f3-f04e5952d73b";


    /*
    *Stub Data for offline workaround
    */
    public const int NUM_OF_ITEMS = 3;
    public const string DEFAULT_MP4_VIDEO_URL = "https://file-examples-com.github.io/uploads/2017/04/file_example_MP4_480_1_5MG.mp4";
    public const string DEFAULT_MP4_VIDEO_URL_2 = "https://www.learningcontainer.com/wp-content/uploads/2020/05/sample-mp4-file.mp4";
    public const string DEFAULT_MP4_VIDEO_URL_3 = "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4";

}
