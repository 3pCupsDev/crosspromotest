using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrossPromoScript : MonoBehaviour
{
    private NetworkManager networkManager;
    private VideoManager videoManager;
    private AnalyticsReporter analyticsReporter;

    void Awake()
    {
        videoManager = GetComponent<VideoManager>();
        networkManager = GetComponent<NetworkManager>();
        analyticsReporter = GetComponent<AnalyticsReporter>();
    }

    void Start()
    {
        CalculatePrefabSize();
        LoadPromos();
    }

    private void CalculatePrefabSize()
    {
        //todo think about resizing
        float height = 200;
        float width = height * Screen.width / Screen.height;
        gameObject.transform.localScale = Vector3.one * height / 6f;
    }

    private void LoadPromos()
    {
        //Make API Call
        PromotionsPlaylistResponse serverResponse = networkManager.GetPlaylist();

        if (serverResponse != null && serverResponse.results.Count > 0)
        {
            List<Promotion> promotionsPlaylist = serverResponse.results;
            videoManager.SubmitList(promotionsPlaylist);
        }
        else
        {
            Debug.Log("Server Error occurred");
        }
    }

    void OnMouseDown()
    {
        Promotion promotion = videoManager.GetCurrentPromotion(); 
        analyticsReporter.ReportClickIfNeeded(promotion.tracking_url);
        if(promotion != null && promotion.click_url != "")
        {
            Application.OpenURL(promotion.click_url);
        } else
        {
            Debug.Log("Error opening the url: " + promotion.click_url);
        }
    }
}
