using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnalyticsReporter : MonoBehaviour
{
    [SerializeField]
    public string PlayerId;

    private NetworkManager networkManager;
    private List<string> reportedUrls = new List<string>();

    void Awake()
    {
        networkManager = GetComponent<NetworkManager>();
    }

    public void ReportClickIfNeeded(string url)
    {
        if(reportedUrls.Contains(url) || url.Equals("")) {
            Debug.Log("No analytics report for url:" + url);
            return;
        }
        string analyticsUrl = url.Replace(Constants.PLAYER_ID_TAG, PlayerId);
        networkManager.ReportAnalytics(analyticsUrl);
        Debug.Log("Reported analytics for url: " + analyticsUrl);
        reportedUrls.Add(url);
    }
}
