using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using System;
using System.IO;

[Serializable]
public class Promotion
{
    public int id;
    public string video_url;
    public string click_url;
    public string tracking_url;
}

[Serializable]
public class PromotionsPlaylistResponse
{
    public List<Promotion> results;
}